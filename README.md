## C# Second Week tasks

1. Task5
Создать иерархию классов Person-Student-Teacher. В каждом классе должны быть свойства, а также виртуальная функция Print и переопределенная функция ToString(). Основная программа создает массив объектов Person или их наследников, после чего выдает его на экран. У каждого Teacher должен быть список Students, которыми он руководит, у каждого Student - Teacher, который им руководит.
Для классов Person-Student-Teacher реализовать и оттестировать ToString(), Equals(), GetHashCode().
Для классов Person-Student-Teacher реализовать статические методы RandomPerson, RandomStudent, RandomTeacher, которые возвращают случайного из некоторого статического массива.
С помощью is, as, GetType определить, сколько в массиве персон, студентов и преподавателей и перевести всех студентов на следующий курс.
Для классов Person-Student-Teacher реализовать глубокое клонирование, определив виртуальный метод Clone(). Клон должен возвращать точную копию по значению и типу. Проиллюстрировать Clone на примере контейнера персон - должны создаваться клоны объекты ровно тех типов, которые содержатся в исходном контейнере.
Используя метод GetType() класса Student и метод BaseType() класса Type, вывести всех предков класса Student (написать общий метод)

2. Task6
Реализовать простейший каталог музыкальных компакт-дисков, который позволяет: добавлять и удалять диски; добавлять и удалять песни; просматривать содержимое целого каталога и каждого диска в отдельности; осуществлять поиск всех записей заданного исполнителя по всему каталогу. 

3. Task8
Разработайте класс который не использует неуправляемые ресурсы и у которого нет финализатора. Разработайте потомок этого класса который использует неуправляемые ресурсы и которому нужен финализатор.
Разработайте класс который использует неуправляемые ресурсы и у которого есть финализатор. Разработайте потомок этого класса который тоже использует неуправляемые ресурсы и которому также нужен финализатор.

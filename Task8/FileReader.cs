﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Task8_
{
    class FileReader:Base, IDisposable
    {
        
        const uint GenericRead = 0x80000000;
        const uint OpenExisting = 3;
        IntPtr _handle;

        [DllImport("kernel32", SetLastError = true)]
        private static extern unsafe IntPtr CreateFile(
              string fileName,                    
              uint desiredAccess,                 
              uint shareMode,                     
              uint securityAttributes,            
              uint creationDisposition,           
              uint flagsAndAttributes,            
              int hTemplateFile                   
              );

        [DllImport("kernel32", SetLastError = true)]
        private static extern unsafe bool ReadFile(
              IntPtr hFile,                      
              void* pBuffer,                    
              int numberOfBytesToRead,           
              int* pNumberOfBytesRead,           
              int overlapped                
              );

        [DllImport("kernel32", SetLastError = true)]
        static extern unsafe bool CloseHandle(IntPtr hObject);

        public bool Open(string fileName)
        {
            // open the existing file for reading          
            _handle = CreateFile(fileName, GenericRead, 0, 0, OpenExisting, 0, 0);
            return _handle != IntPtr.Zero;
        }

        public unsafe int Read(byte[] buffer, int index, int count)
        {
            var n = 0;
            fixed (byte* p = buffer)
            {
                if (!ReadFile(_handle, p + index, count, &n, 0))
                    return 0;
            }
            return n;
        }

        private bool Close()
        {
            // close file`s handle
            return CloseHandle(_handle);
        }

        protected override void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                Close();
            }
            _disposed = true;
        }
    }
}

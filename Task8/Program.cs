﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task8_
{
    class Program
    {
        static void Main(string[] args)
        {
            var buffer = new byte[128];
            using (var fr = new FileReader())
            {
                if (fr.Open("..\\..\\test.txt"))
                {
                    // Подразумевается, что происходит чтение файла в кодировке ASCII
                    var encoding = new UTF8Encoding();
                    int bytesRead;
                    do
                    {
                        bytesRead = fr.Read(buffer, 0, buffer.Length);
                        var content = encoding.GetString(buffer, 0, bytesRead);
                        Console.Write("{0}", content);
                    }
                    while (bytesRead > 0);

                    fr.Dispose();
                }
                else
                {
                    Console.WriteLine("Не получилось открыть запрашиваемый файл");
                }
            }

            Console.ReadKey();
        }
    }
}

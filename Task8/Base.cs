﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task8_
{
    class Base
    {
        protected bool _disposed = false;
        public void Dispose()
        {
            Console.WriteLine("\nDispose called");
            Dispose(true);
            //finalize won`t call for this instance
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _disposed = true;
        }
    }
}

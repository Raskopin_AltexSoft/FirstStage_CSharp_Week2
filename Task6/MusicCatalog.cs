﻿using System;
using System.Collections.Generic;

namespace Task6
{
    class MusicCatalog
    {
        private static MusicCatalog _instance;

        public List<Musician> Catalog { get; } = new List<Musician>();

        //realize singleton pattern
        public static MusicCatalog GetInstance(List<Musician> musicians)
        {
            if (_instance == null)
                _instance = new MusicCatalog(musicians);
            return _instance;
        }

        private MusicCatalog() { }
        public static MusicCatalog GetInstance()
        {
            if (_instance == null)
                _instance = new MusicCatalog();
            return _instance;
        }

        private MusicCatalog(List<Musician> musicians)
        {
            Catalog = musicians;
        }

        //get all albums from catalog
        public List<Album> GetAllAlbums()
        {
            var albums = new List<Album>();
            foreach (var musician in Catalog)
            {
                if (musician.Albums != null)
                    albums.AddRange(musician.Albums);
            }
            return albums;
        }

        //get all songs from catalog
        public List<string> GetAllSongs()
        {
            var songs = new List<string>();
            foreach (var album in GetAllAlbums())
            {
                songs.AddRange(album.Songs);
            }
            return songs;
        }

        //find musician by name in catalog
        public Musician FindMusicianByName(string name)
        {
            return Catalog.Find(musicianFromList => name.ToLower() == musicianFromList.Name.ToLower());
        }

        //find album by title in catalog
        public Album FindAlbumByTitle(string title)
        {
            return GetAllAlbums().Find(albumFromList => title.ToLower() == albumFromList.Name.ToLower());
        }
    }
}

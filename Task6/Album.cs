﻿using System.Collections.Generic;


namespace Task6
{
    internal class Album
    {
        public List<string> Songs { get; set; }
        public string Name { get; }
        public Musician Musician { get; }

        public void AddSong(string song)
        {
            Songs.Add(song);
        }

        public bool DeleteSong(string song)
        {
            return Songs.Remove(song);
        }

        public Album(string name, Musician musician)
        {
            Name = name;
            Musician = musician;
            musician.AddAlbum(this);
        }

        public bool IfSongExist(string title)
        {
            return Songs.FindIndex(s => title == s) >= 0;
        }
    }
}

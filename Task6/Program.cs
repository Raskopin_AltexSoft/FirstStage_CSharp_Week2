﻿using System;

namespace Task6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var engine = Engine.GetInstance();
            engine.MainMenu();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}

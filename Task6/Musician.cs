﻿using System.Collections.Generic;

namespace Task6
{
    internal class Musician
    {
        public string Name { get; set; }
        private readonly List<Album> _albums = new List<Album>();
        public List<Album> Albums { get { return _albums; } }

        //add album to musician
        public void AddAlbum(Album album)
        {
            var albumIndex = FindAlbum(album);
            if (albumIndex < 0)
                _albums.Add(album);
        }

        //remove album from musician
        public void RemoveAlbum(Album album)
        {
            var albumIndex = FindAlbum(album);
            if (albumIndex >= 0)
                _albums.RemoveAt(albumIndex);
        }

        //find album from musician. If found, then return index of album
        public int FindAlbum(Album album)
        {
            if (_albums != null)
                return _albums.FindIndex(albumFromList => album.Name == albumFromList.Name);
            return -1;
        }

        //check if album exists by it title
        public bool IfAlbumExist(string title)
        {
            foreach (var album in _albums)
            {
                if (title == album.Name)
                    return true;
            }
            return false;
        }

        //get all songs by musician
        public List<string> GetAllSongs()
        {
            var songs = new List<string>();
            foreach(var album in _albums)
            {
                songs.AddRange(album.Songs);
            }
            return songs;

        }

        public Musician(string name)
        {
            Name = name;
        }
    }
}

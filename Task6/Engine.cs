﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Task6
{
    internal class Engine
    {
        private static Engine _instance;

        //realize singleton pattern
        public static Engine GetInstance()
        {
            if (_instance == null)
                _instance = new Engine();
            return _instance;
        }

        //fill music catalog
        private Engine()
        {
            FillCatalog();
        }

        private MusicCatalog _catalog;

        //this function fill musician catalog
        private void FillCatalog()
        {
            var musicians = new List<Musician>();
            var pinkFloyd = new Musician("Pink Floyd");
            var kingCrimson = new Musician("King Crimson");
            var genesis = new Musician("Genesis");
            var yes = new Musician("Yes");
            var porcupineTree = new Musician("Porcupine Tree");
            var elp = new Musician("ELP");

            var wishYouWereHere = new Album("Wish you were here", pinkFloyd);
            var darkSid = new Album("Dark side of the Moon", pinkFloyd);
            var inTheCourt = new Album("In the court of crimson king", kingCrimson);
            var red = new Album("Red", kingCrimson);
            var foxtrot = new Album("Foxtrot", genesis);
            var sellingEngland = new Album("Selling England by the pound", genesis);
            var closeToTheEdge = new Album("Close to the edge", yes);
            var lightbulbSun = new Album("Lithbulb Sun", porcupineTree);
            var incident = new Album("Incident", porcupineTree);

            wishYouWereHere.Songs = new List<string> {
                "Shine on you crazy diamonds: part I",
                "Shine on you crazy diamonds: part II",
                "Shine on you crazy diamonds: part III",
                "Wish you were here" };
            darkSid.Songs = new List<string> {
                "Speak to me / Breathe",
                "On the run",
                "Time",
                "Money"};
            inTheCourt.Songs = new List<string> {
                "21st Century Schizoid Man",
                "I talk to the wild",
                "Epitaph",
                "Moonchild",
                "The court of crimson king"};
            red.Songs = new List<string> {
                "Red",
                "Fallen angel",
                "One more red nightmare",
                "Providence",
                "Starless"};
            foxtrot.Songs = new List<string> {
                "Watchers of the skies",
                "Time table",
                "Horizons"};
            sellingEngland.Songs = new List<string> {
                "Dancing with the moonlit knight",
                "Firth of Fifth",
                "More fool me",
                "The cinema show"};
            closeToTheEdge.Songs = new List<string> {
                "Close to the edge",
                "And you and i",
                "Siberian Khatru"};
            lightbulbSun.Songs = new List<string> {
                "Lightbulb sun",
                "Russia on ice",
                "Shesmovedon",
                "The rest will flow",
                "Hatesong"};
            incident.Songs = new List<string> {
                "Occam`s razor",
                "The blind house",
                "The incident",
                "Time flies"};

            pinkFloyd.AddAlbum(wishYouWereHere);
            pinkFloyd.AddAlbum(darkSid);
            kingCrimson.AddAlbum(inTheCourt);
            kingCrimson.AddAlbum(red);
            genesis.AddAlbum(foxtrot);
            genesis.AddAlbum(sellingEngland);
            yes.AddAlbum(closeToTheEdge);
            porcupineTree.AddAlbum(lightbulbSun);
            porcupineTree.AddAlbum(incident);
            musicians.Add(pinkFloyd);
            musicians.Add(kingCrimson);
            musicians.Add(genesis);
            musicians.Add(yes);
            musicians.Add(porcupineTree);
            musicians.Add(elp);
            _catalog = MusicCatalog.GetInstance(musicians);
        }

        //main program menu
        public void MainMenu()
        {
            byte[] menuValues = { 1, 2, 3, 4, 5, 6, 7 };
            byte userInput;
            Console.WriteLine("Choose one of items:");
            Console.WriteLine("1: Show all exist musicians;");
            Console.WriteLine("2: Show all exist albums;");
            Console.WriteLine("3: Show all exist songs;");
            Console.WriteLine("4: Show all songs by given musician;");
            Console.WriteLine("5: Show all songs by given album;");
            Console.WriteLine("6: Add or Remove;");
            Console.WriteLine("7: Program exit;");

            while (!byte.TryParse(Console.ReadLine(), out userInput) || !menuValues.Contains<byte>(userInput))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            MainMenuHandelr(userInput);
        }

        //second menu: remove and add items
        public void AddRemove()
        {
            byte[] menuValues = { 1, 2, 3, 4, 5, 6, 7 };
            byte userInput;
            Console.WriteLine("Choose one of items:");
            Console.WriteLine("1: Add musician;");
            Console.WriteLine("2: Remove musician;");
            Console.WriteLine("3: Add album;");
            Console.WriteLine("4: Remove album;");
            Console.WriteLine("5: Add song;");
            Console.WriteLine("6: Remove song;");
            Console.WriteLine("7: Back to main menu;");

            while (!byte.TryParse(Console.ReadLine(), out userInput) || !menuValues.Contains<byte>(userInput))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            AddRemoveMenuHandler(userInput);
        }

        //this method handle user`s answer in Add/Remove menu
        public void AddRemoveMenuHandler(byte choice)
        {
            Console.Clear();
            switch (choice)
            {
                case 1:
                    AddMusician();
                    MainMenu();
                    break;
                case 2:
                    RemoveMusician();
                    MainMenu();
                    break;
                case 3:
                    AddAlbum();
                    MainMenu();
                    break;
                case 4:
                    RemoveAlbum();
                    MainMenu();
                    break;
                case 5:
                    AddSong();
                    MainMenu();
                    break;
                case 6:
                    RemoveSong();
                    MainMenu();
                    break;
                case 7:
                    MainMenu();
                    break;
                default:
                    break;
            }

        }

        //this method handle user`s answer in main menu
        public void MainMenuHandelr(byte choice)
        {
            Console.Clear();
            switch (choice)
            {
                case 1:
                    OutputAllMusician();
                    Console.WriteLine();
                    MainMenu();
                    break;
                case 2:
                    OutputAllAlbums();
                    Console.WriteLine();
                    MainMenu();
                    break;
                case 3:
                    OutputAllSongs();
                    Console.WriteLine();
                    MainMenu();
                    break;
                case 4:
                    FindMusicianSongsByMusician();
                    Console.Clear();
                    MainMenu();
                    break;
                case 5:
                    FindMusicianSongsByAlbum();
                    Console.Clear();
                    MainMenu();
                    break;
                case 6:
                    AddRemove();
                    Console.Clear();
                    MainMenu();
                    break;
                case 7:
                    break;
                default:
                    break;
            }
        }

        //output to console list of musicians
        public void OutputAllMusician()
        {
            if (_catalog.Catalog.Count <= 0) return;
            Console.WriteLine("List of musicians contain in _catalog:");
            foreach (var musician in _catalog.Catalog)
            {
                Console.WriteLine(" " + musician.Name);
            }
        }

        //output to console list of albums
        public void OutputAllAlbums()
        {
            Console.WriteLine("  List of albums contain in _catalog:");
            foreach (var album in _catalog.GetAllAlbums())
            {
                Console.WriteLine("{0} by {1}", album.Name, album.Musician.Name);
            }
        }

        //output to console list of songs
        public void OutputAllSongs()
        {
            Console.WriteLine("  List of songs contain in _catalog");
            foreach (var album in _catalog.GetAllAlbums())
            {
                foreach (var song in album.Songs)
                {
                    Console.WriteLine("{0} by {1} from {2} album", song, album.Musician.Name, album.Name);
                }
            }
        }

        //this method return songs by given musician
        public void FindMusicianSongsByMusician()
        {
            Console.WriteLine("Please, input musician name:");
            var name = Console.ReadLine();
            var musician = _catalog.FindMusicianByName(name);
            if (musician != null)
            {
                if (musician.GetAllSongs().Count == 0)
                {
                    Console.WriteLine("Given musician don't has songs");
                }
                else
                {
                    Console.WriteLine("   List of songs:");
                    foreach (var song in musician.GetAllSongs())
                    {
                        Console.WriteLine(song);
                    }
                }
            } 
            else
            {
                Console.WriteLine("Not foud musician with given name.");
            }
            if (Continue())
            {
                FindMusicianSongsByMusician();
            }
        }

        //this method return songs by given album title
        public void FindMusicianSongsByAlbum()
        {
            Console.WriteLine("Please, input album title:");
            var title = Console.ReadLine();
            var album = _catalog.FindAlbumByTitle(title);
            if (album != null)
            {
                Console.WriteLine("Musician for given album: {0}", album.Musician.Name);
                if (album.Songs.Count == 0)
                {
                    Console.WriteLine("Given album don't has songs");
                }
                else
                {
                    Console.WriteLine("   List of songs:");
                    foreach (var song in album.Songs)
                    {
                        Console.WriteLine(song);
                    }
                }
            }
            else
            {
                Console.WriteLine("Not found album with given title.");
            }
            if (Continue())
            {
                FindMusicianSongsByAlbum();
            }
        }

        //this method handle user`s answer to question continue work with menu or no
        public bool Continue()
        {
            Console.WriteLine("Do you want to continue? Y/N");
            var answer = Console.ReadLine();
            if (answer != "y" && answer != "Y" && answer != "N" && answer != "n")
            {
                Continue();
            }
            return answer == "Y" || answer == "y";
        }
         
        //this menu add musician to catalog
        public void AddMusician()
        {
            Console.WriteLine("Please, input musician name:");
            var name = Console.ReadLine();
            if (_catalog.FindMusicianByName(name) == null)
            {
                _catalog.Catalog.Add(new Musician(name));
                Console.WriteLine("Musician {0} successfuly added.", name);
            }
            else
            {
                Console.WriteLine("Musician {0} already exists.", name);
            }
            if (Continue())
            {
                Console.Clear();
                AddRemove();
            }
            Console.Clear();
        }

        //this menu remove musician to catalog
        public void RemoveMusician()
        {
            Console.WriteLine("Please, choose:");
            var choose = ChooseMusician();
            if (choose == -1)
                Console.WriteLine("No musicians in _catalog");
            else
                if (choose > -1)
            {
                _catalog.Catalog.RemoveAt(choose);
                Console.WriteLine("Successfly delee musician");
            }
            if (Continue())
            {
                Console.Clear();
                AddRemove();
            }
        }

        //this menu add album to catalog
        public void AddAlbum()
        {
            var choose = ChooseMusician();
            if (choose == -1)
                Console.WriteLine("No musicians in _catalog");
            else
                if (choose > -1)
            {
                Console.WriteLine("Please, input album title:");
                var title = Console.ReadLine();
                if (!_catalog.Catalog[choose].IfAlbumExist(title))
                {
                    var album = new Album(title, _catalog.Catalog[choose]);
                    Console.WriteLine("Album {0} successfuly added.", title);
                }
                else
                {
                    Console.WriteLine("Album {0} already exists.", title);
                }
            }
            if (Continue())
            {
                Console.Clear();
                AddRemove();
            }
        }

        //this menu remove album from catalog
        public void RemoveAlbum()
        {
            var choose = ChooseMusician();
            if (choose == -1)
                Console.WriteLine("No musicians in _catalog");
            else
                if (choose > -1)
            {
                var musician = _catalog.Catalog[choose];
                choose = ChooseAlbum(musician);
                if (choose == -1)
                    Console.WriteLine("No albums from given musician");
                else
                if (choose > -1)
                {
                    musician.RemoveAlbum(musician.Albums[choose]);
                    Console.WriteLine("Successfly delete album");
                }
            }
            if (Continue())
            {
                Console.Clear();
                AddRemove();
            }
        }

        //this menu add song to catalog
        public void AddSong()
        {
            var choose = ChooseMusician();
            if (choose == -1)
                Console.WriteLine("No musicians in _catalog");
            else
                if (choose > -1)
            {
                var musician = _catalog.Catalog[choose];
                choose = ChooseAlbum(musician);
                if (choose == -1)
                    Console.WriteLine("No albums from given musician");
                else
                if (choose > -1)
                {
                    Console.WriteLine("Please, input song title:");
                    var title = Console.ReadLine();
                    if (!musician.Albums[choose].IfSongExist(title))
                    {
                        musician.Albums[choose].AddSong(title);
                        Console.WriteLine("Song {0} successfuly added.", title);
                    }
                    else
                    {
                        Console.WriteLine("Song {0} already exists.", title);
                    }
                }
            }
            if (Continue())
            {
                Console.Clear();
                AddRemove();
            }
        }

        //this menu remove song from catalog
        public void RemoveSong()
        {
            var choose = ChooseMusician();
            if (choose == -1)
                Console.WriteLine("No musicians in _catalog");
            else
                if (choose > -1)
            {
                var musician = _catalog.Catalog[choose];
                choose = ChooseAlbum(musician);
                if (choose == -1)
                    Console.WriteLine("No albums from given musician");
                else
                if (choose > -1)
                {
                    choose = ChooseSong(musician.Albums[choose]);
                    if (choose == -1)
                        Console.WriteLine("No songs from given album");
                    else
                    if (choose > -1)
                    {
                        musician.Albums[choose].Songs.RemoveAt(choose);
                        Console.WriteLine("Successfly delete song");
                    }
                }
            }
            if (Continue())
            {
                Console.Clear();
                AddRemove();
            }
        }

        //this method realize user`s choose of musician
        public int ChooseMusician()
        {
            if (_catalog.Catalog.Count == 0)
                return -1;
            var allowSymbols = new List<string> {"X", "x"};
            var i = 0;
            foreach (var musician in _catalog.Catalog)
            {
                i++;
                allowSymbols.Add(i.ToString());
                Console.WriteLine("{0}. {1};", i, musician.Name);
            }
            string output;
            Console.WriteLine("Press X to exit this menu.");
            while (!allowSymbols.Contains(output = Console.ReadLine()))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return int.Parse(output)-1;
        }

        //this method realize user`s choose of album
        public int ChooseAlbum(Musician musician)
        {
            if (musician.Albums.Count == 0)
                return -1;
            var allowSymbols = new List<string> {"X", "x"};
            var i = 0;
            foreach (var album in musician.Albums)
            {
                i++;
                allowSymbols.Add(i.ToString());
                Console.WriteLine("{0}. {1};", i, album.Name);
            }
            string output;
            Console.WriteLine("Press X to exit this menu.");
            while (!allowSymbols.Contains(output = Console.ReadLine()))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return int.Parse(output) - 1;
        }

        //this method realize user`s choose of song
        public int ChooseSong(Album album)
        {
            if (album.Songs.Count == 0)
                return -1;
            var allowSymbols = new List<string> {"X", "x"};
            var i = 0;
            foreach (var song in album.Songs)
            {
                i++;
                allowSymbols.Add(i.ToString());
                Console.WriteLine("{0}. {1};", i, song);
            }
            string output;
            Console.WriteLine("Press X to exit this menu.");
            while (!allowSymbols.Contains(output = Console.ReadLine()))
            {
                Console.WriteLine("Value is wrong. Please, try again:");
            }
            return int.Parse(output) - 1;
        }
    }
}

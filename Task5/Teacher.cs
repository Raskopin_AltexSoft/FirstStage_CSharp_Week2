﻿using System;
using System.Collections.Generic;

namespace Task5
{
    class Teacher: Person
    {
        public string Subject { get; }
        private readonly List<Student> _students = new List<Student>();

        public Teacher(string firstName, string lastName, DateTime birthday, string subject)
            :base(firstName, lastName, birthday)
        {
            Subject = subject;
        }

        //constructor with Address set
        public Teacher(string firstName, string lastName, DateTime birthday, string subject, string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
            :base(firstName, lastName, birthday, country, town, street, buildingNumber, apartmentNumber)
        {
            Subject = subject;
        }

        //constructor with class Address and list of _students in arguments
        public Teacher(string firstName, string lastName, DateTime birthday, Address address, string subject, List<Student> students)
            : base(firstName, lastName, birthday, address)
        {
            Subject = subject;
            this._students = students;
        }

        //define method Clone for ICloneable interface
        public override object Clone()
        {
            return new Teacher(this.FirstName, this.LastName, this.Birthday, this.Address, this.Subject, this._students); 
        }

        //add student to _students list
        public void AddStudent(Student student)
        {
            _students.Add(student);
        }

        //return _students list
        public List<Student> GetStudents()
        {
            return _students;
        }

        //this method wait object with IPrintable type and calls method Print than define in IPrintable interface
        public override void Print(IPrintable teacher)
        {
            teacher.Print(this);
        }

        //override virtual method ToString
        public override string ToString()
        {
            return FullName + "; Subject: " + Subject + "; Number of _students: " + _students.Count;
        }

        //override virtual method Equals
        public override bool Equals(Object obj)
        {
            if (obj == null || obj.GetType() != typeof (Teacher)) return false;
            var teacher = obj as Teacher;
            return teacher != null && (FullName == teacher.FullName && Address.FullAddres == teacher.Address.FullAddres && Subject == teacher.Subject);
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode() ^ Subject.GetHashCode();
        }

        //return random person from persons list
        public new static Teacher Random(List<Person> persons)
        {
            var random = new Random();
            var t = new List<Teacher>();
            foreach (var person in persons)
            {
                if (person.GetType() == typeof(Teacher))
                    t.Add(person as Teacher);
            }
            try
            {
                return t[random.Next(0, t.Count)];
            }
            catch
            {
                throw new Exception("collection is empty");
            }
        }
    }
}

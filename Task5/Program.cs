﻿using System;


namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            var engine = Engine.GetInstance();

            var persons = engine.SetList();
            var randomPerson = Person.Random(persons);
            var randomTeacher = Teacher.Random(persons);
            var randomStudent = Student.Random(persons);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("First task: output list of persons which include persons, students and teachers");
            Console.ResetColor();
            engine.ListOut(persons);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Second task: get random person, random teacher, random student from the list of persons:");
            Console.ResetColor();
            randomPerson.Print(engine);
            randomTeacher.Print(engine);
            randomStudent.Print(engine);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Third task: Count which classes objects in list have (with three different way):");
            Console.ResetColor();
            Engine.CountOf<Person>(persons);
            Console.WriteLine();
            Engine.CountOf<Student>(persons);
            Console.WriteLine();
            Engine.CountOf<Teacher>(persons);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Fourth task: Check deep clone for persons in list (show new list after cloned and show what types exist in new list):");
            Console.ResetColor();
            var newPersons = engine.ClonePerson(persons);
            engine.ListOut(newPersons);
            Engine.CountOf<Person>(newPersons);
            Console.WriteLine();
            Engine.CountOf<Student>(newPersons);
            Console.WriteLine();
            Engine.CountOf<Teacher>(newPersons);

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Fifth task: Check equals method:");
            Console.ResetColor();
            Console.WriteLine("Objects with all the same: " + persons[0].Equals(persons[1]));
            Console.WriteLine("Objects with different apartment number: " + persons[0].Equals(persons[2]));

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Sixth task: Check gethashcode method:");
            Console.ResetColor();
            Console.WriteLine("Objects with all the same: " + persons[0].GetHashCode() + " " +persons[1].GetHashCode());
            Console.WriteLine("Objects with different apartment number: " + persons[0].GetHashCode() + " " + persons[2].GetHashCode());

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Seventh task: get base type of class Student");
            Console.ResetColor();
            Console.WriteLine(Student.Random(persons).GetType().BaseType);
            Console.WriteLine(Engine.GetBaseTypes<Student>());
            Console.ReadKey();
        }
    }
}

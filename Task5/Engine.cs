﻿using System;
using System.Collections.Generic;

namespace Task5
{
    class Engine : IPrintable
    {

        private static Engine _instance;

        private Engine()
        { }

        public static Engine GetInstance()
        {
            if (_instance == null)
                _instance = new Engine();
            return _instance;
        }

        readonly List<Person> _persons = new List<Person>();

        public List<Person> SetList()
        {
            var makarenko = new Teacher("Anton", "Makarenko", new DateTime(1888, 1, 13), "Pedagogical", "Russian Empire", "Bilopillia", "Center street", 7);
            //for this object and object above we later will check gethashcode and equal
            var makarenkoRepeat = new Teacher("Anton", "Makarenko", new DateTime(1888, 1, 13), "Pedagogical", "Russian Empire", "Bilopillia", "Center street", 7);
            //same teacher with wrong Address for chek equals method
            var wrongMakarenko = new Teacher("Anton", "Makarenko", new DateTime(1888, 1, 13), "Pedagogical", "Russian Empire", "Bilopillia", "Center street", 8);
            var einstein = new Teacher("Albert ", "Einstein", new DateTime(1879, 4, 18), "Physical", "German Empire", "Ulm", "Kaiser street", 10, 17);
            var newton = new Teacher("Isaac", "Newton", new DateTime(1642, 3, 20), "Physical", "England", "Woolsthorpe", "Shakespeare street", 5);
            var confucius = new Teacher("Confucius", "", new DateTime(551, 09, 28), "Philosophy", "China", "Zou", "Center", 1);

            var tolstoy = new Person("Leo", "Tolstoy", new DateTime(1828, 9, 9), "Russian Impire", "Yasnaya Polyana", "Sunny street", 20);
            //same person with wrong birthday for check equals and gethashcode method
            var wrongTolstoy = new Person("Leo", "Tolstoy", new DateTime(1828, 9, 10), "Russian Impire", "Yasnaya Polyana", "Sunny street", 20);
            //for this person and object above we later will check gethashcode and equal
            var tolstoyRepeat = new Person("Leo", "Tolstoy", new DateTime(1828, 9, 9), "Russian Impire", "Yasnaya Polyana", "Sunny street", 20); 
            var dostoeyevsky = new Person("Fyodor", "Dostoyevsky", new DateTime(1846, 11, 11), "Russian Impire", "Moscow", "Tverskaya street", 50, 8);
            var chekhov = new Person("Anton", "Chekhov", new DateTime(1860, 7, 15), "Russian Impire", "Taganrog", "Green boulevard", 2);
            var faulkner = new Person("William", "Faulkner", new DateTime(1897, 9, 25), "USA", "New Albany", "Second Avenue", 91, 55);

            var afanasenko = new Student("Vasya", "Afanasenko", new DateTime(1988, 1, 25), "Pedagogical", makarenko, "Ukraine", "Kremenchuk", "Sobornaya street", 9, 5, 5);
            var goldarev = new Student("Aleksandr", "Goldarev", new DateTime(1992, 4, 2), "Pedagogical", makarenko, "Ukraine", "Kremenchuk", "Pushkina boulevard", 2, 5);
            var goldarev2 = new Student("Aleksandr2", "Goldarev", new DateTime(1992, 4, 2), "Pedagogical", makarenko, "Ukraine", "Kremenchuk", "Pushkina boulevard", 2, 5);
            var potavec = new Student("Sofia", "Potavec", new DateTime(1970, 10, 28), "Pedagogical", wrongMakarenko, "Ukraine", "Kremenchuk", "Second Avenue", 10, 15);
            var petrova = new Student("Olga", "Petrova", new DateTime(1989, 2, 15), "Physical", einstein, "Ukraine", "Poltava", "Pochtova ploscha", 13, 7);
            var kotikov = new Student("Boris", "Kotikov", new DateTime(1992, 7, 25), "Physical", einstein, "Ukraine", "Poltava", "Green street", 9, 8);
            var rolina = new Student("Elizaveta", "Rolina", new DateTime(1993, 1, 13), "Philosophy", confucius, "Ukraine", "Globino", "Kolhoznaya street", 11, 102);
            var korjenko = new Student("Mikhail", "Korjenko", new DateTime(1985, 5, 10), "Philosophy", confucius, "Ukraine", "Dnepr", "Main", 3, 17);
            var alekseev = new Student("Bogdan", "Alekseev", new DateTime(1990, 5, 17), "Physical", newton, "Ukraine", "Dnepr", "Second tupik", 22, 1);
            var ivanov = new Student("Vasya", "Ivanov", new DateTime(1988, 8, 5), "Pedagogical", makarenko, "Ukraine", "Kiev", "Kreschatik street", 5, 7);

            _persons.Add(makarenko);
            _persons.Add(makarenkoRepeat);
            _persons.Add(wrongMakarenko);
            _persons.Add(einstein);
            _persons.Add(newton);
            _persons.Add(confucius);

            _persons.Add(tolstoy);
            _persons.Add(wrongTolstoy);
            _persons.Add(tolstoyRepeat);
            _persons.Add(dostoeyevsky);
            _persons.Add(chekhov);
            _persons.Add(faulkner);

            _persons.Add(afanasenko);
            _persons.Add(goldarev);
            _persons.Add(goldarev2);
            _persons.Add(potavec);
            _persons.Add(petrova);
            _persons.Add(kotikov);
            _persons.Add(rolina);
            _persons.Add(korjenko);
            _persons.Add(alekseev);
            _persons.Add(ivanov);

            return _persons;
        }

        public void Print(Person person)
        {
            Console.WriteLine(person.ToString());
        }

        public static void CountOf<T>(List<Person> persons) where T : class
        {
            var i = 0;
            var j = 0;
            var k = 0;
            foreach (var person in persons)
            {
                if (person is T)
                {
                    i++;
                }
                var p = person as T;
                if (p != null)
                {
                    j++;
                }
                if (person.GetType() == typeof(T))
                {
                    k++;
                }
            }
            Console.WriteLine("Acount of "+ typeof(T) +" class with operator \"is\": {0}", i);
            Console.WriteLine("Acount of " + typeof(T) + " class with operator \"as\": {0}", j);
            Console.WriteLine("Acount of " + typeof(T) + " class operator \"GetType\": {0}", k);
        }

        public void ListOut(List<Person> persons)
        {
            foreach (var person in persons)
            {
                person.Print(this);
            }
        }

        public void GetHashCodes(List<Person> persons)
        {
            foreach (var person in persons)
            {
                Console.WriteLine(person.GetHashCode());
            }
        }

        public List<Person> ClonePerson(List<Person> persons)
        {
            var newPersons = new List<Person>();
            foreach (var person in persons)
            {
                newPersons.Add((Person)person.Clone());
            }
            return newPersons;
        }

        public static string GetBaseTypes<T>()
        {
            var type = typeof(T);
            var typeName = type.ToString();
            type = type.BaseType;
            while (type != null)
            {
                typeName += "=>" + type.ToString();
                type = type.BaseType;
            }
            return typeName;
        }
    }
}

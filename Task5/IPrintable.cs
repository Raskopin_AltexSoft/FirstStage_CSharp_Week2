﻿namespace Task5
{
    internal interface IPrintable
    {
        void Print(Person person);
    }
}

﻿using System;
using System.Collections.Generic;


namespace Task5
{
    internal class Person: ICloneable
    {
        protected readonly Address Address;
        public bool IsSet { get; set; }
        public bool IsAddresSet { get; set;  }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public DateTime Birthday { get; }

        //constructor without Address set
        public Person(string firstName, string lastName, DateTime birthday)
        {
            IsSet = false;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
        }

        //constructor with Address set
        public Person(string firstName, string lastName, DateTime birthday, string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
            :this(firstName, lastName, birthday)
        {
            Address = new Address
            {
                Country = country,
                Town = town,
                Street = street,
                BuildingNumber = buildingNumber,
                ApartmentNumber = apartmentNumber
            };
            IsSet = true;
        }

        //construtor with class Address as argument
        public Person(string firstName, string lastName, DateTime birthday, Address address)
            : this(firstName, lastName, birthday)
        {
            this.Address = address;
        }

        //method for Address changing
        public void ChangeAddress(string country, string town, string street, short buildingNumber, short apartmentNumber = 0)
        {
            Address.Country = country;
            Address.Town = town;
            Address.Street = street;
            Address.BuildingNumber = buildingNumber;
            Address.ApartmentNumber = apartmentNumber;
        }

        //this method wait object with IPrintable type and calls method Print than define in IPrintable interface
        public virtual void Print(IPrintable person)
        {
            person.Print(this);
        }


        //override virtual method ToString
        public override string ToString()
        {
            return FullName;
        }

        //override virtual method Equals
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof (Person)) return false;
            var person = obj as Person;
            return person != null && (FullName == person.FullName && Address.FullAddres == person.Address.FullAddres);
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode()  ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode();
        }

        //define method Clone for ICloneable interface
        public virtual object Clone()
        {
            var p = new Person(this.FirstName, this.LastName, this.Birthday, this.Address);
            return p;
        }

        //return random person from persons list
        public static Person Random(List<Person> persons)
        {
            var random = new Random();
            var p = new List<Person>();
            foreach (var person in persons)
            {
                if (person.GetType() == typeof(Person))
                    p.Add(person);
            }
            try
            {
                return p[random.Next(0, p.Count)];
            }
            catch
            {
                throw new Exception("collection is empty");
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;

namespace Task5
{
    internal class Student : Person
    {
        private byte _course;
        public string Faculty { get; set; }

        public byte Course
        {
            get { return _course; }
            set
            {
                _course = 1;
                if (value > 0 && value < 6)
                {
                    _course = value;
                }
            }
        }

        public Teacher Teacher { get; set; }

        //constructor with faculty and teacher in arguments
        public Student(string firstName, string lastName, DateTime birthday, string faculty, Teacher teacher, byte course = 1)
            : base(firstName, lastName, birthday)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
            teacher.AddStudent(this);
        }

        //constructor with Address set
        public Student(string firstName, string lastName, DateTime birthday, string faculty, Teacher teacher,
            string country, string town, string street, short buildingNumber, short apartmentNumber = 0, byte course = 1)
            :base(firstName, lastName, birthday, country, town, street, buildingNumber, apartmentNumber)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
            teacher.AddStudent(this);
        }

        //constructor with class Address in arguments.This constructor for clone method
        public Student(string firstName, string lastName, DateTime birthday, Address address, string faculty, Teacher teacher, byte course = 1)
        : base(firstName, lastName, birthday, address)
        {
            Faculty = faculty;
            Course = course;
            Teacher = teacher;
        }

        //define method Clone for ICloneable interface
        public override object Clone()
        {
            var p = new Student(this.FirstName, this.LastName, this.Birthday, this.Address, this.Faculty, this.Teacher, this.Course);
            return p;
        }

        //this method wait object with IPrintable type and calls method Print than define in IPrintable interface
        public override void Print(IPrintable student)
        {
            student.Print(this);
        }

        //override virtual method ToString
        public override string ToString()
        {
            return FullName + "; Faculty: " + Faculty + "; Teacher: " +Teacher.FullName;
        }

        //override virtual method GetHashCode. return xor of properties hashes that take part in Equals method
        public override int GetHashCode()
        {
            return FullName.GetHashCode() ^ Birthday.GetHashCode() ^ Address.FullAddres.GetHashCode() ^ Faculty.GetHashCode();
        }

        public new static Student Random(List<Person> persons)
        {
            var random = new Random();
            var s = new List<Student>();
            foreach (var person in persons)
            {
                if (person.GetType() == typeof(Student))
                    s.Add(person as Student);
            }
            try
            {
                return s[random.Next(0, s.Count)];
            }
            catch
            {
                throw new Exception("collection is empty");
            }
        }

        //override virtual method Equals
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof (Student)) return false;
            var student = obj as Student;
            return student != null && (FullName == student.FullName && Address.FullAddres == student.Address.FullAddres
                                       && Faculty == student.Faculty && Teacher.Equals(student.Teacher));
        }
    }
}
